#!/usr/bin/env python
from setuptools import (
        setup,
        find_packages
        )

setup(name='blog-builder',
      version='1.0',
      description='A python app to build, deploy a simple static flask app',
      author='Cameron Lane',
      author_email='cameron@adamanteus.com',
      url='https://github.com/crlane/cameronlane.org',
      packages=find_packages(exclude=['tests', 'tests.*']),
      include_package_data=True,
      classifiers=["Private :: Do Not Upload"],
      scripts=['cli/sitebuilder.py']
      )
