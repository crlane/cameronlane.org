title: home
published: True
type: page
tags: 

# Welcome.  

## Biography
I am a husband, father, and programmer. 

I like computer science and baseball. I write here, mostly about things related to those two topics. I used to be a teacher, so I might write things about that as well.

## Disclaimer
This is my homepage.  The thoughts, words, and opinions shared here are my own and are not reviewed or endorsed by any organizations with which I am associated. 



